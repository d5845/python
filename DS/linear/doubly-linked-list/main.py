"""Doubly Linked List"""

class Node:
    def __init__(self, data):
        self.data = data
        self.previous = None
        self.next = None


class DLL:
    def __init__(self):
        self.head = None
        self.tail = None

    def display(self, reverse=False):
        if self.head == self.tail == None:
            return
        current_firstnode, current_lastnode = self.head, self.tail

        while (current_firstnode or current_lastnode) is not None:
            if reverse == True:
                print(current_lastnode.data, end="->")
                current_lastnode = current_lastnode.previous
            else:
                print(current_firstnode.data, end="->")
                current_firstnode = current_firstnode.next
        print()

    def append(self, data):
        if self.head == self.tail == None:
            self.head = self.tail = Node(data)
            return

        current_node = self.head
        while current_node.next is not None:
            current_node = current_node.next
        new_node = Node(data)
        new_node.previous = current_node
        current_node.next = new_node
        self.tail=new_node
        


doubly_linked_list = DLL()

n = int(input("Enter number of operations : "))

for _ in range(n):
    data = input(f"{_+1} Data : ")
    doubly_linked_list.append(data)

print("FORWARD LIST")
doubly_linked_list.display(reverse=False)
print("REVERSE LIST")
doubly_linked_list.display(reverse=True)
